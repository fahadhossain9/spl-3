import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import GitlabProvider from "next-auth/providers/gitlab";
console.log(process.env.GOOGLE_CLIENT_ID,'red');
export const authOptions = {
    providers: [
        GoogleProvider({
            clientId: process.env.GOOGLE_CLIENT_ID || '',
            clientSecret: process.env.GOOGLE_CLIENT_SECRET || '',
        }),
        GitlabProvider({
            clientId: process.env.GITLAB_CLIENT_ID || '',
            clientSecret: process.env.GITLAB_CLIENT_SECRET || '',
        })
    ],
  secret: process.env.SECRET,
}

export default NextAuth(authOptions);