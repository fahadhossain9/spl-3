// chatApi.ts

import { apiSlice } from "../api/apiSlice";

export const chatApi = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    fetchUserMessages: builder.query({
      query: (userId) => ({
        url: `user/${userId}/messages`, // Update the URL to match your backend route
        method: "GET",
        credentials: "include" as const,
      }),
    }),
    fetchAdminMessages: builder.query({
      query: () => ({
        url: "admin/messages", // Update the URL to match your backend route
        method: "GET",
        credentials: "include" as const,
      }),
    }),
    sendMessage: builder.mutation({
      query: ({ userId, role, content }) => ({
        url: "messages", // Update the URL to the correct endpoint
        method: "POST",
        body: { userId, role, content }, // Adjust the body structure based on your backend model
        credentials: "include" as const,
      }),
    }),
  }),
});

export const { useFetchUserMessagesQuery, useFetchAdminMessagesQuery, useSendMessageMutation } = chatApi;
