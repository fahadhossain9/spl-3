import React, { FC, useEffect, useState } from "react";
import YouTube from "react-youtube";
import ReactPlayer from "react-player";
import axios from "axios";

type Props = {
  videoUrl: string;
  title: string;
};

const CoursePlayer: FC<Props> = ({ videoUrl }) => {
  const [videoId, setVideoId] = useState<string | null>(null);
  const [isYouTube, setIsYouTube] = useState<boolean>(false);

  useEffect(() => {
    // Extract video ID from the YouTube URL
    const url = new URL(videoUrl);
    const videoIdParam = url.searchParams.get("v");

    if (videoIdParam) {
      setVideoId(videoIdParam);
      setIsYouTube(true);
    } else {
      setIsYouTube(false);
    }
  }, [videoUrl]);

  return (
    <div className="relative w-full">
      {isYouTube ? (
        videoId && (
          <div className="mr-10 border-2 border-black dark:border-white rounded-md">
            <YouTube
              videoId={videoId}
              opts={{
                width: "100%",
                playerVars: {
                  autoplay: 0,
                  controls: 1,
                },
              }}
            />
          </div>
        )
      ) : (
        <div className="mr-10 border-2 border-black dark:border-white h-[400px] rounded-md">
          <ReactPlayer
            url={videoUrl}
            width="100%"
            height="100%"
            controls={true}
          />
        </div>
      )}
    </div>
  );
};

export default CoursePlayer;
