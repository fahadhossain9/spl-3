import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Link from "next/link";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

interface Option {
  _id: string;
  text: string;
  isCorrect: boolean;
}

interface MCQ {
  _id: string;
  question: string;
  options: Option[];
}

interface ExamData {
  _id: string;
  course_Id: string;
  courseTitle: string;
  examTitle: string;
  examId: string;
  mcqs: MCQ[];
  __v: number;
}

interface ExamPreviewProps {
  id: string;
}

const PreviewQuestions: React.FC<ExamPreviewProps> = ({ id }) => {
  const [examData, setExamData] = useState<ExamData | null>(null);
  const [selectedOptions, setSelectedOptions] = useState<
    Record<string, string | null>
  >({});
  const [loading, setLoading] = useState(false);
  const { user } = useSelector((state: any) => state.auth);
  const [submissionCompleted, setSubmissionCompleted] = useState(false);
  const userId = user._id;

  useEffect(() => {
    const fetchExamData = async () => {
      try {
        const response = await fetch(
          `http://localhost:8000/api/v1/getExam/${id}`
        );

        if (!response.ok) {
          throw new Error("Failed to fetch exam data");
        }

        const data: ExamData = await response.json();
        setExamData(data);
        initializeSelectedOptions(data);
      } catch (error) {
        console.error("Error fetching exam data:", error);
      }
    };

    const initializeSelectedOptions = (examData: ExamData) => {
      const initialSelectedOptions: Record<string, string | null> = {};
      examData.mcqs.forEach((mcq) => {
        initialSelectedOptions[mcq._id] = null;
      });
      setSelectedOptions(initialSelectedOptions);
    };

    fetchExamData();
  }, [id]);

  const handleOptionChange = (mcqId: string, optionId: string) => {
    setSelectedOptions((prev) => ({ ...prev, [mcqId]: optionId }));
  };

  const handleSubmit = async () => {
    try {
      setLoading(true);

      const answers: Record<
        string,
        {
          selected: string | null;
          correct: string[];
          question: string;
          options: { id: string; text: string }[];
        }
      > = {};

      examData?.mcqs.forEach((mcq) => {
        const selected = selectedOptions[mcq._id] || null;
        const correct = mcq.options.find((option) => option.isCorrect)?._id;

        answers[mcq._id] = {
          selected,
          correct: correct ? [correct] : [],
          question: mcq.question,
          options: mcq.options.map((option) => ({
            id: option._id,
            text: option.text,
          })),
        };
      });

      const submissionData = {
        course_Id: examData?.course_Id,
        courseTitle: examData?.courseTitle,
        examTitle: examData?.examTitle,
        userId: userId,
        mcqs: answers,
      };

      const response = await fetch("http://localhost:8000/api/v1/postresults", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(submissionData),
      });

      if (!response.ok) {
        throw new Error("Failed to submit answers");
      }

      toast.success("Answers submitted successfully!");
    } catch (error) {
      console.error("Error submitting answers:", error);
      toast.error("Failed to submit answers");
    } finally {
      setLoading(false);
      setSubmissionCompleted(true);
    }
  };

  if (!examData) {
    return <div>Loading...</div>;
  }

  return (
    <>
     {submissionCompleted && (
        <div className="mt-6 pb-6 flex items-center justify-center gap-x-6">
          <Link
            href="/examList"
            className="text-sm font-semibold leading-6 text-indigo-600 underline"
          >
            All Exams
          </Link>
          <Link
            href="/showallresults"
            className="text-sm font-semibold leading-6 text-indigo-600 underline"
          >
            Show All Results
          </Link>
        </div>
      )}
      <div className="flex items-center justify-center ">
        <form className="w-96">
          <div className="border-b border-gray-900/10 pb-12">
            <h2 className="text-xxl font-semibold leading-7 flex justify-center text-gray-900">
              Exam Preview
            </h2>

            <h3 className="text-lg font-medium leading-6 text-gray-900 mb-4">
              {examData.examTitle}
            </h3>

            <div className="mt-10 space-y-6">
              {examData.mcqs.map((mcq) => (
                <div
                  key={mcq._id}
                  className="col-span-full border border-dashed border-gray-900/25 rounded-lg p-6"
                >
                  <h3 className="text-lg font-medium leading-6 text-gray-900 mb-4">
                    {mcq.question}
                  </h3>
                  {mcq.options.map((option) => (
                    <div key={option._id} className="flex items-center mb-2">
                      <input
                        type="radio"
                        id={`option_${option._id}`}
                        name={`mcq_${mcq._id}`}
                        checked={selectedOptions[mcq._id] === option._id}
                        onChange={() => handleOptionChange(mcq._id, option._id)}
                        className="mr-2"
                      />
                      <label
                        htmlFor={`option_${option._id}`}
                        className="text-sm text-gray-900"
                      >
                        {option.text}
                      </label>
                    </div>
                  ))}
                </div>
              ))}

              <div className="mt-6 flex items-center justify-end gap-x-6">
                <button
                  type="button"
                  className="text-sm font-semibold leading-6 text-gray-900"
                >
                  Cancel
                </button>
                <button
                  type="button"
                  onClick={handleSubmit}
                  className={`rounded-md px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 ${
                    loading ? "bg-gray-400 cursor-not-allowed" : "bg-indigo-600"
                  }`}
                  disabled={loading}
                >
                  {loading ? "Submitting..." : "Submit Answers"}
                </button>
              </div>
            </div>
          </div>
        </form>

        <ToastContainer position="bottom-right" autoClose={5000} />
      </div>
    </>
  );
};

export default PreviewQuestions;
