"use client";
import React, { useState } from "react";
import axios from "axios";

interface Option {
  text: string;
  isCorrect: boolean;
}

interface MCQ {
  question: string;
  options: Option[];
}

interface ExamFormProps {
  onSubmit: (examData: {
    course_Id: string;
    courseTitle: string;
    examTitle: string;
    mcqs: MCQ[];
  }) => void;
}

const ExamForm = (props: any) => {
  const [courseId, setCourseId] = useState("");
  const [courseTitle, setCourseTitle] = useState("");
  const [examTitle, setExamTitle] = useState("");
  const [mcqs, setMCQs] = useState<MCQ[]>([
    { question: "", options: [{ text: "", isCorrect: false }] },
  ]);

  const handleAddMCQ = () => {
    setMCQs((prevMCQs) => [
      ...prevMCQs,
      { question: "", options: [{ text: "", isCorrect: false }] },
    ]);
  };

  const handleAddOption = (mcqIndex: number) => {
    setMCQs((prevMCQs) => {
      const newMCQs = [...prevMCQs];
      newMCQs[mcqIndex].options.push({ text: "", isCorrect: false });
      return newMCQs;
    });
  };

  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    mcqIndex?: number,
    optionIndex?: number
  ) => {
    if (mcqIndex === undefined) {
      // Handle course and exam details
      switch (event.target.id) {
        case "courseId":
          setCourseId(event.target.value);
          break;
        case "courseTitle":
          setCourseTitle(event.target.value);
          break;
        case "examTitle":
          setExamTitle(event.target.value);
          break;
        default:
          break;
      }
    } else {
      // Handle MCQ details
      setMCQs((prevMCQs) => {
        const newMCQs = [...prevMCQs];
        if (optionIndex !== undefined) {
          newMCQs[mcqIndex].options[optionIndex].text = event.target.value;
        } else {
          newMCQs[mcqIndex].question = event.target.value;
        }
        return newMCQs;
      });
    }
  };

  const handleCheckboxChange = (mcqIndex: number, optionIndex: number) => {
    setMCQs((prevMCQs) => {
      const newMCQs = [...prevMCQs];
      newMCQs[mcqIndex].options[optionIndex].isCorrect =
        !newMCQs[mcqIndex].options[optionIndex].isCorrect;
      return newMCQs;
    });
  };
  const handleSubmit = async () => {
    try {
      // Validate the form fields (add your own validation logic)
      if (
        !courseId ||
        !courseTitle ||
        !examTitle ||
        mcqs.some(
          (mcq) => !mcq.question || mcq.options.some((option) => !option.text)
        )
      ) {
        // Handle validation error (you can display an error message)
        console.error(
          "Please fill in all fields for course and exam details, and provide questions and options for each MCQ."
        );
        return;
      }

      // Prepare exam data
      const examData = {
        course_Id: courseId,
        courseTitle,
        examTitle,
        mcqs,
      };

      // Make the API request
      const response = await axios.post(
        "http://localhost:8000/api/v1/createExam",
        examData
      );

      // Handle the response (you can show a success message, etc.)
      console.log("Exam created successfully:", response.data);
    } catch (error) {
      // Handle error (you can display an error message)
      console.error("Error creating exam:", error);
    }
  };

  return (
    <div className="flex items-center justify-center my-10">
      <form className="w-96">
        <div className="border-b border-gray-900/10 pb-12">
          <h2 className="text-xxl font-semibold leading-7 flex justify-center text-gray-900">
            Exam Form
          </h2>

          <div className="mt-10 space-y-6">
            {/* Course ID */}
            <div className="col-span-full border border-dashed border-gray-900/25 rounded-lg p-6">
              <label
                htmlFor="courseId"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Course ID
              </label>
              <input
                type="text"
                id="courseId"
                name="courseId"
                className="block w-full px-1 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                value={courseId}
                onChange={(e) => handleChange(e)}
              />
            </div>

            {/* Course Title */}
            <div className="col-span-full border border-dashed border-gray-900/25 rounded-lg p-6">
              <label
                htmlFor="courseTitle"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Course Title
              </label>
              <input
                type="text"
                id="courseTitle"
                name="courseTitle"
                className="block w-full px-1 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                value={courseTitle}
                onChange={(e) => handleChange(e)}
              />
            </div>

            {/* Exam Title */}
            <div className="col-span-full border border-dashed border-gray-900/25 rounded-lg p-6">
              <label
                htmlFor="examTitle"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Exam Title
              </label>
              <input
                type="text"
                id="examTitle"
                name="examTitle"
                className="block w-full px-1 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                value={examTitle}
                onChange={(e) => handleChange(e)}
              />
            </div>

            {mcqs.map((mcq, mcqIndex) => (
              <div
                key={mcqIndex}
                className="col-span-full border border-dashed border-gray-900/25 rounded-lg p-6"
              >
                <label
                  htmlFor={`mcq-question-${mcqIndex}`}
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Question
                </label>
                <input
                  type="text"
                  id={`mcq-question-${mcqIndex}`}
                  name={`mcq-question-${mcqIndex}`}
                  className="block w-full px-1 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  value={mcq.question}
                  onChange={(e) => handleChange(e, mcqIndex)}
                />

                <label className="block mt-4 text-sm font-medium leading-6 text-gray-900">
                  Options
                </label>
                {mcq.options.map((option, optionIndex) => (
                  <div
                    key={optionIndex}
                    className="flex items-center space-x-2 mt-2"
                  >
                    <input
                      type="text"
                      id={`mcq-option-${mcqIndex}-${optionIndex}`}
                      name={`mcq-option-${mcqIndex}-${optionIndex}`}
                      className="block w-3/4 px-1 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      value={option.text}
                      onChange={(e) => handleChange(e, mcqIndex, optionIndex)}
                    />
                    <input
                      type="checkbox"
                      id={`mcq-option-correct-${mcqIndex}-${optionIndex}`}
                      name={`mcq-option-correct-${mcqIndex}-${optionIndex}`}
                      checked={option.isCorrect}
                      onChange={() =>
                        handleCheckboxChange(mcqIndex, optionIndex)
                      }
                    />
                    <label
                      htmlFor={`mcq-option-correct-${mcqIndex}-${optionIndex}`}
                      className="text-sm text-gray-900"
                    >
                      Correct
                    </label>
                  </div>
                ))}
                <button
                  type="button"
                  className="mt-2 text-sm text-indigo-600 hover:text-indigo-500"
                  onClick={() => handleAddOption(mcqIndex)}
                >
                  + Add Option
                </button>
              </div>
            ))}

            {/* Add MCQ Button */}
            <button
              type="button"
              className="mt-2 text-sm text-indigo-600 hover:text-indigo-500"
              onClick={handleAddMCQ}
            >
              + Add MCQ
            </button>
          </div>

          {/* Submit Button */}
          <div className="mt-6 flex items-center justify-end gap-x-6">
            <button
              type="button"
              className="text-sm font-semibold leading-6 text-gray-900"
            >
              Cancel
            </button>
            <button
              type="button"
              onClick={handleSubmit}
              className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              Submit Exam
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default ExamForm;
