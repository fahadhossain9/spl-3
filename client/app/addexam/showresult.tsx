// Import necessary libraries and icons
import React, { useEffect, useState } from "react";
import { FaCheck, FaTimes } from "react-icons/fa";
import Link from "next/link";

// Interface for the Result Data
interface ResultData {
  course_Id: string;
  courseTitle: string;
  examTitle: string;
  userId: string;
  mcqs: Record<
    string,
    {
      selected: string;
      correct: string[];
      question: string;
      options: { id: string; text: string }[];
    }
  >;
}

// Interface for ResultPreviewProps
interface ResultPreviewProps {
  resultId: string;
}

// ResultPreview Component
const ResultPreview: React.FC<ResultPreviewProps> = ({ resultId }) => {
  const [resultData, setResultData] = useState<ResultData | null>(null);

  useEffect(() => {
    // Fetch result data based on resultId
    const fetchResultData = async () => {
      try {
        const response = await fetch(`http://localhost:8000/api/v1/results/${resultId}`);
        
        if (!response.ok) {
          throw new Error('Failed to fetch result data');
        }

        const data: ResultData = await response.json();
        setResultData(data);
        console.log(data);
      } catch (error) {
        console.error('Error fetching result data:', error);
      }
    };

    fetchResultData();
  }, [resultId]);

  if (!resultData || !resultData.mcqs) {
    return <div>Loading...</div>;
  }

  return (
    <>
       <div className="mt-6 pb-6 flex items-center justify-center gap-x-6">
          <Link
            href="/showallresults"
            className="text-sm font-semibold leading-6 text-indigo-600 underline"
          >
            All Result
          </Link>
          <Link
            href="/profile"
            className="text-sm font-semibold leading-6 text-indigo-600 underline"
          >
            Go to Profile
          </Link>
        </div>
    <div className="flex items-center justify-center ">
      <div className="w-96">
        <div className="border-b border-gray-900/10 pb-12">
          <h2 className="text-xxl font-semibold leading-7 flex justify-center text-gray-900">
            Result Preview
          </h2>

          {/* Display Exam Title */}
          <h3 className="text-lg font-medium leading-6 text-gray-900 mb-4">
            {resultData.examTitle}
          </h3>

          <div className="mt-10 space-y-6">
            {Object.keys(resultData.mcqs).map((mcqId) => (
              <div
                key={mcqId}
                className="col-span-full border border-dashed border-gray-900/25 rounded-lg p-6"
              >
                <h3 className="text-lg font-medium leading-6 text-gray-900 mb-4">
                  {resultData.mcqs[mcqId].question}
                </h3>
                {resultData.mcqs[mcqId].options.map((option) => (
                  <div key={option.id} className="flex items-center mb-2">
                    {/* Radio button for options */}
                    <input
                      type="radio"
                      id={`option_${option.id}`}
                      name={`mcq_${mcqId}`}
                      checked={resultData.mcqs[mcqId].selected === option.id}
                      readOnly
                      className="mr-2"
                    />
                    {/* Label for options */}
                    <label
                      htmlFor={`option_${option.id}`}
                      className={`text-sm text-gray-900 ${
                        resultData.mcqs[mcqId].correct.includes(option.id)
                          ? "text-green-500" // Correct Answer (Green)
                          : resultData.mcqs[mcqId].selected === option.id
                          ? "text-red-500"   // Incorrect Answer (Red) if selected but not correct
                          : ""               // No color if not selected and not correct
                      }`}
                    >
                      {option.text}
                    </label>
                    {/* Display Right or Cross Icon based on correctness */}
                    {resultData.mcqs[mcqId].selected === option.id && !resultData.mcqs[mcqId].correct.includes(option.id) && (
                      <FaTimes className="text-red-500 ml-2" />
                    )}
                    {resultData.mcqs[mcqId].selected === option.id && resultData.mcqs[mcqId].correct.includes(option.id) && (
                      <FaCheck className="text-green-500 ml-2" />
                    )}
                  </div>
                ))}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
    </>
  );
};

export default ResultPreview;
