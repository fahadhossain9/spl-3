// Import necessary libraries
import React, { useEffect, useState } from "react";
import Link from "next/link";

// Interface for Exam
interface Exam {
  _id: any;
  examId: string;
  examTitle: string;
  courseTitle: string;
}

// ExamList Component
const ExamList: React.FC = () => {
  const [exams, setExams] = useState<Exam[] | null>(null);

  // Fetch exams data
  useEffect(() => {
    const fetchExams = async () => {
      try {
        const response = await fetch("http://localhost:8000/api/v1/examlist");

        if (!response.ok) {
          throw new Error("Failed to fetch exam data");
        }

        const data: Exam[] = await response.json();
        setExams(data);
      } catch (error) {
        console.error("Error fetching exam data:", error);
      }
    };

    fetchExams();
  }, []);

  if (!exams) {
    return <div>Loading...</div>;
  }

  return (
    <div className="flex flex-wrap justify-center min-h-[400px] items-center ">
      {exams.length === 0 ? (
        <div className="text-center">
          <p>No exams available.</p>
          <Link href="/addexam">
            <a className="text-blue-500 underline">Add an exam</a>
          </Link>
        </div>
      ) : (
        exams.map((exam) => (
          <Link key={exam.examId} href={`/attend-exam/${exam._id}`}>
            <div className="max-w-md min-w-sm hover:scale-110  m-4 bg-white p-4 rounded-md shadow-md cursor-pointer">
              <h3 className="text-xl font-semibold mb-2">{exam.examTitle}</h3>
              <p className="text-gray-600">{exam.courseTitle}</p>
            </div>
          </Link>
        ))
      )}
    </div>
  );
};

export default ExamList;
