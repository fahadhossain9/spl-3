import React from "react";
import { styles } from "../styles/style";
import Image from "next/image"; // Import the Image component from next/image

const About = () => {
  return (
    <div className="text-black dark:text-white">
      <br />
      <h1 className={`${styles.title} text-[2.5rem] md:text-4xl lg:text-[45px]`}>
        About <span className="text-gradient">Binary Physics</span>
      </h1>

      <br />
      <div className="flex flex-col md:flex-row mx-4 md:mx-10 justify-between">
        <div className="w-full md:w-[60%] m-auto md:mr-4">
          <p className="text-[16px] md:text-[20px] lg:text-lg ">
            {`
            Hello! My name is Fahad, and I'm currently a student at the Indian
            Institute of Technology, Dhaka University. I have a passion for
            teaching, specializing in Physics and Information and Communication
            Technology (ICT). As an online educator, I strive to make learning
            engaging and accessible. I believe in the power of education to
            transform lives and create a positive impact. In addition to my
            academic pursuits, I enjoy exploring the intersections of science and
            technology.
            `}
          </p>
          <br />
          <p className="text-[16px] md:text-[20px] lg:text-lg ">
            {`
          My goal is to inspire and empower students to develop a
            strong foundation in these subjects, preparing them for success in
            their academic and professional journeys. If you have any questions or
            would like to know more about my teaching approach, feel free to reach
            out!
            `}
          </p>
          <br />
          <span className="text-[20px] md:text-[24px] font-semibold">Fahad Hossain</span>
          <h5 className="text-[16px] md:text-[20px] lg:text-lg ">
            Founder and CEO of Binary Physics
          </h5>
          <br />
          <br />
          <br />
        </div>
        <div className="w-full md:w-[40%] flex justify-center mt-4 md:mt-0">
          <Image
            src={`/fahad.jpg`}
            alt={`fahad`}
            width={400}
            height={400}
            className="w-full h-full md:w-[400px] md:h-[400px] object-cover rounded-lg"
          />
        </div>
      </div>
    </div>
  );
};

export default About;
