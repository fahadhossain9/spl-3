// Import necessary libraries
"use client"
import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useSelector } from "react-redux";

// ResultList Component
const ResultList: React.FC = () => {
  const { user } = useSelector((state: any) => state.auth);
  const userId = user._id;
  const [results, setResults] = useState<any[] | null>(null);

  // Fetch result IDs data
  useEffect(() => {
    const fetchResultIds = async () => {
      try {
        const response = await fetch(`http://localhost:8000/api/v1/results/ids/${userId}`);

        if (!response.ok) {
          throw new Error("Failed to fetch result IDs");
        }

        const data: any[] = await response.json();
        setResults(data);
      } catch (error) {
        console.error("Error fetching result IDs:", error);
      }
    };

    fetchResultIds();
  }, [userId]);

  if (!results) {
    return <div>Loading...</div>;
  }

  return (
    <div className=" mx-[5%] ">

    <div className="flex flex-wrap justify-start min-h-[400px] items-center">
      {results.length === 0 ? (
        <div className="text-center">
          <p>No results available.</p>
        </div>
      ) : (
        results.map((result) => (
          <Link key={result.id} href={`/show-result/${result.id}`}>
            <div className="max-w-md min-w-sm hover:scale-110 m-4 bg-white p-4 rounded-md shadow-md cursor-pointer">
              <h3 className="text-xl font-semibold mb-2">{result.examTitle}</h3>
              <p className="text-gray-600">{result.courseTitle}</p>
            </div>
          </Link>
        ))
      )}
    </div>
    </div>
  );
};

export default ResultList;
