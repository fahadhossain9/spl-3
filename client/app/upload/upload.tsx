"use client";
import React, { useState } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const CustomDropzone = () => {
  const [selectedFile, setSelectedFile] = useState<File | null>(null); // Explicitly specify the type as File | null
  const [title, setTitle] = useState("");
  const [loading, setLoading] = useState(false);

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0] || null;
    setSelectedFile(file);
  };

  const handleTitleChange = (event: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setTitle(event.target.value);
  };

  const handleUpload = async () => {
    if (selectedFile && title) {
      const formData = new FormData();
      formData.append("videoFile", selectedFile);
      formData.append("title", title);

      try {
        setLoading(true);

        const response = await axios.post(
          "http://localhost:8000/api/v1/upload",
          formData,
          {
            headers: { "Content-Type": "multipart/form-data" },
          }
        );

        console.log(
          `${selectedFile.name} uploaded successfully. Server response:`,
          response.data
        );

        toast.success(`${selectedFile.name} uploaded successfully`);
      } catch (error) {
        console.error("Error uploading file:", error);
        toast.error("Error uploading file");
      } finally {
        setLoading(false);
      }
    } else {
      console.error("Title and file are required");
      toast.error("Title and file are required");
    }
  };

  return (
    <div className="flex items-center justify-center ">
      <form className="w-96">
        <div className="border-b border-gray-900/10 dark:border-white pb-12">
          <h2 className="text-3xl font-semibold leading-7 flex justify-center dark:text-white text-gray-900">
            Video Upload
          </h2>

          <div className="mt-10 space-y-6">
            <div className="col-span-full border border-dashed border-gray-900/25 dark:border-white rounded-lg p-6">
              <label
                htmlFor="file-upload"
                className="block text-sm font-medium leading-6 dark:text-white text-gray-900"
              >
                Video File
              </label>
              <div className="mt-2 flex justify-center">
                <div className="text-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6 dark:bg-white"
                  >
                    <path
                      strokeLinecap="round"
                      d="M15.75 10.5l4.72-4.72a.75.75 0 011.28.53v11.38a.75.75 0 01-1.28.53l-4.72-4.72M4.5 18.75h9a2.25 2.25 0 002.25-2.25v-9a2.25 2.25 0 00-2.25-2.25h-9A2.25 2.25 0 002.25 7.5v9a2.25 2.25 0 002.25 2.25z"
                    />
                  </svg>

                  <div className="mt-4 flex text-sm leading-6 text-gray-600 dark:text-white">
                    <label
                      htmlFor="file-upload"
                      className="relative cursor-pointer rounded-md bg-white font-semibold text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-indigo-500"
                    >
                      <span>Upload a file</span>
                      <input
                        id="file-upload"
                        name="file-upload"
                        type="file"
                        className="sr-only"
                        onChange={handleFileChange}
                      />
                    </label>
                    <p className="pl-1">or drag and drop</p>
                  </div>
                  <p className="text-xs leading-5 text-gray-600 dark:text-white">
                    Accepted formats: video/*
                  </p>
                </div>
              </div>
            </div>

            <div className="col-span-full">
              <label
                htmlFor="video-title"
                className="block text-sm font-medium leading-6 dark:text-white text-gray-900"
              >
                Title
              </label>
              <div className="mt-2">
                <input
                  type="text"
                  id="video-title"
                  name="video-title"
                  className="block w-full px-1 rounded-md border-0 py-1.5 dark:text-white text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  value={title}
                  onChange={handleTitleChange}
                />
              </div>
            </div>
          </div>

          <div className="mt-6 flex items-center justify-end gap-x-6">
            <button
              type="button"
              className="text-sm font-semibold leading-6 dark:text-white text-gray-900"
            >
              Cancel
            </button>
            <button
              type="button"
              onClick={handleUpload}
              className={`rounded-md px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 ${
                loading ? "bg-gray-400 cursor-not-allowed" : "bg-indigo-600"
              }`}
              disabled={loading}
            >
              {loading ? "Uploading..." : "Upload"}
            </button>
          </div>
        </div>
      </form>
      <ToastContainer position="bottom-right" autoClose={5000} />
    </div>
  );
};

export default CustomDropzone;
