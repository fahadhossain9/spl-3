'use client'
import React from "react";
import ResultPreview from "@/app/addexam/showresult";


const Page = ({params}:any) => {
    return (
        <div>
            <ResultPreview resultId={params.id} />
        </div>
    )
}

export default Page;
 