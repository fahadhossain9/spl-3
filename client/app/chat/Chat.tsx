import React, { useEffect, useState } from "react";

type ChatProps = {
  role: string;
  content: string;
  avatar:string
};

const Chat: React.FC<ChatProps> = ({ role, content,avatar }) => {
  const [displayedContent, setDisplayedContent] = useState<string>("");
  let typingTimeout: NodeJS.Timeout | undefined;

  useEffect(() => {
    let currentIndex = 0;
    const typingDelay = role === "admin" ? 0 : 0;

    const typeCharacter = () => {
      if (currentIndex < content.length - 1) {
        setDisplayedContent((prevContent) => prevContent + content[currentIndex]);
        currentIndex++;
        typingTimeout = setTimeout(typeCharacter, typingDelay);
      }
    };

    if (typingTimeout) {
      clearTimeout(typingTimeout);
    }

    if (typingDelay === 0) {
      setDisplayedContent(content);
    } else {
      typeCharacter();
    }

    return () => {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
    };
  }, [role, content]);

  return (
    <div className={`flex justify-center w-full py-[1rem] border-b-[1px] ${role !== "user" ? "bg-[#F7F7F8]" : ""}`}>
      <div className="flex w-[70%]">
        <img
          src={role === "user" ? avatar : "/admin.png"}
          alt={role}
          className="w-[30px] h-[30px] rounded-full mr-[20px]"
        />
        <div className="flex flex-wrap items-center">
          <pre
            id="chat-container"
            className="flex-wrap content-[12px] leading-6"
            style={{ overflowWrap: "break-word", whiteSpace: "pre-wrap" }}
          >
            {displayedContent}
          </pre>
        </div>
      </div>
    </div>
  );
};

export default Chat;
