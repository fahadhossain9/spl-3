"use client";
import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Chat from "./Chat";
import { useSelector } from "react-redux";
import socketIO from "socket.io-client";

const ENDPOINT = process.env.NEXT_PUBLIC_SOCKET_SERVER_URI || "";
const socket = socketIO(ENDPOINT, { transports: ["websocket"] });

interface ChatAppMessagesProps {
  userId: string;
}

const ChatAppMessages: React.FC<ChatAppMessagesProps> = ({ userId }) => {
  const [avatar, setAvatar] = useState("");
  const [chat, setChat] = useState<Array<{ role: string; content: string }>>(
    []
  );
  const [message, setMessage] = useState("");
  const chatContainerRef = useRef<HTMLDivElement | null>(null);

  const { user } = useSelector((state: any) => state.auth);
  const role = user.role;
  const [audio] = useState<any>(
    typeof window !== "undefined" &&
      new Audio(
        "https://res.cloudinary.com/damk25wo5/video/upload/v1693465789/notification_vcetjn.mp3"
      )
  );

  const playNotificationSound = () => {
    audio.play();
  };
  useEffect(() => {
    const fetchData = async () => {
      if (userId) {
        setAvatar(user.avatar.url);
        try {
          const response = await axios.get(
            `http://localhost:8000/api/v1/user/${userId}/messages`
          );
          setChat(response.data.messages);
        } catch (error) {
          console.error("Error fetching messages:", error);
        }
      }
    };

    fetchData();

    // Cleanup: Remove the Socket.IO event listener when the component unmounts
    return () => {
      socket.off("chat message");
    };
  }, [userId, user.avatar.url]);
  // adminchat.tsx
  useEffect(() => {
    socket.on("chat message", (newMessage) => {
      console.log("Received gusti kilai new message:", newMessage);
      setChat((prevChat) => [...prevChat, newMessage]);
      playNotificationSound();
    });

    return () => {
      socket.off("chat message");
    };
  }, []);

  const sendMessage = async () => {
    try {
      const newMessage = { role: role, content: message, userId: userId };
      console.log(newMessage);
      await axios.post(`http://localhost:8000/api/v1/messages`, newMessage);
      setMessage("");
      // Emit the new message to the server
      socket.emit("chat message", newMessage);
    } catch (error) {
      console.error("Error sending message:", error);
    }
  };
  const scrollToBottom = () => {
    if (chatContainerRef.current) {
      chatContainerRef.current.scrollTop =
        chatContainerRef.current.scrollHeight;
    }
  };

  useEffect(() => {
    scrollToBottom();
  }, [chat]);

  return (
    <>
      <div className="flex flex-col  w-3/4 mx-auto ">
        <div
          className="w-full  h-[500px] border-2 flex flex-col items-center  flex-grow overflow-y-scroll overflow-x-hidden  pt-[2rem]"
          ref={chatContainerRef}
        >
          {chat.length === 0 ? (
            <div className="text-center dark:text-white text-gray-500">
              A message to start your conversation
            </div>
          ) : (
            chat.map((data, index) => (
              <Chat
                key={index}
                role={data.role}
                content={data.content}
                avatar={avatar}
              />
            ))
          )}
        </div>
        <div className="flex items-center justify-center  w-full h-[10%] p-4 border-t-[1px]">
          <input
            type="text"
            className="w-1/2 p-2 mr-2 border border-gray-300 rounded"
            placeholder="Type your message..."
            value={message}
            onChange={(e) => setMessage(e.target.value)}
          />
          <button
            className="px-4 py-2 flex items-center  text-white bg-[#5046e5] rounded"
            onClick={sendMessage}
          >
            Send
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
              className="w-5 h-5 ml-2"
            >
              <path
                fill-rule="evenodd"
                d="M4.72 3.97a.75.75 0 011.06 0l7.5 7.5a.75.75 0 010 1.06l-7.5 7.5a.75.75 0 01-1.06-1.06L11.69 12 4.72 5.03a.75.75 0 010-1.06zm6 0a.75.75 0 011.06 0l7.5 7.5a.75.75 0 010 1.06l-7.5 7.5a.75.75 0 11-1.06-1.06L17.69 12l-6.97-6.97a.75.75 0 010-1.06z"
                clip-rule="evenodd"
              />
            </svg>
          </button>
        </div>
      </div>
    </>
  );
};

export default ChatAppMessages;
