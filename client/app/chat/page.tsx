"use client";
import React, { useState } from "react";
import Heading from "../utils/Heading";
import Header from "../components/Header";
import ChatApp from "./ChatApp";
import AdminChat from "./AdminChat";
import { useSelector } from "react-redux";

type Props = {};

const Page = (props: Props) => {
  const [open, setOpen] = useState(false);
  const [activeItem, setActiveItem] = useState(4);
  const [route, setRoute] = useState("Login");
  const { user } = useSelector((state: any) => state.auth);
  const userId = user._id;
  const role = user.role;
  return (
    <div className="min-h-screen">
      <Heading
        title="Chat- Binary Physics"
        description="Binary Physics is a learning management system for helping programmers."
        keywords="programming,mern"
      />
      <Header
        open={open}
        setOpen={setOpen}
        activeItem={activeItem}
        setRoute={setRoute}
        route={route}
      />
      <br />
      {role === "user" ? (
        <ChatApp />
      ) : role === "admin" ? (
        <AdminChat />
      ) : (
        // Display message in the center of the screen when role is null
        <div className=" text-[40px] flex items-center justify-center h-screen  pb-[200px]">
          <p>Please log in to access the chat.</p>
        </div>
      )}
    </div>
  );
};

export default Page;
