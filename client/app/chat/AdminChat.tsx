// AdminChat Component
import React, { useState, useEffect } from "react";
import axios from "axios";
import ChatAppMessages from "./ChatAppMessages";

const ENDPOINT = process.env.NEXT_PUBLIC_SOCKET_SERVER_URI || "";
const socket = socketIO(ENDPOINT, { transports: ["websocket"] });
import socketIO from "socket.io-client";

const AdminChat = () => {
  const [selectedUserId, setSelectedUserId] = useState<string | null>(null);
  const [chatList, setChatList] = useState<string[]>([]);

  useEffect(() => {
    const fetchChatList = async () => {
      try {
        const response = await axios.get(
          "http://localhost:8000/api/v1/admin/messages"
        );
        const userIds = response.data.messages.map(
          (userMessage: { userId: string }) => userMessage.userId
        );
        setChatList(userIds);
      } catch (error) {
        console.error("Error fetching chat list:", error);
      }
    };

    fetchChatList();
  }, []);

  const handleChatClick = (userId: string) => {
    setSelectedUserId(userId);
  };

  return (
    <div className="flex">
      <div className="w-1/4 p-4 border-r-[1px]">
        <h2 className="text-xl font-semibold mb-4">Chats</h2>
        {chatList.map((chatUserId) => (
          <div
            key={chatUserId}
            className={`cursor-pointer p-2 mb-2 rounded ${
              selectedUserId === chatUserId ? "bg-gray-200" : ""
            }`}
            onClick={() => handleChatClick(chatUserId)}
          >
            {/* Display user information or customize as needed */}
            {chatUserId}
          </div>
        ))}
      </div>
      <div className="w-3/4 h-screen p-4 ">
        {selectedUserId ? (
          <ChatAppMessages userId={selectedUserId} />
        ) : (
          <div className="text-center dark:text-white text-gray-500">
            Select a chat to start your conversation
          </div>
        )}
      </div>
    </div>
  );
};

export default AdminChat;
