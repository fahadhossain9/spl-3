import { styles } from "@/app/styles/style";
import Image from "next/image";
import React from "react";
import ReviewCard from "../Review/ReviewCard";

type Props = {};
export const reviews = [
  {
    name: "Sakib",
    avatar: "https://randomuser.me/api/portraits/men/1.jpg",
    profession: "Student | IIT",
    comment:
      "Impressive courses on Binary physics! Extensive range of tech-related topics for different skill levels. Highly recommended!",
  },
  {
    name: "Mohsin",
    avatar: "https://randomuser.me/api/portraits/women/1.jpg",
    profession: "Student | IIT",

    comment:
      "Amazing programming tutorials! Outstanding teaching style, top-notch quality, and practical applications. Thank you for your dedication!",
  },
  {
    name: "Siam",
    avatar: "https://randomuser.me/api/portraits/men/2.jpg",
    profession: "Student | IIT",

    comment:
      "Fantastic programming tutorials! Impressive ability to break down complex topics. Valuable real-world examples and insights. Keep up the fantastic work!",
  },
  {
    name: "Rahat",
    avatar: "https://randomuser.me/api/portraits/women/2.jpg",
    profession: "Student | IIT",

    comment:
      "Thoroughly impressed with Binary physics! Extensive range of courses on various tech-related topics. Great experience!",
  },
  {
    name: "Siam",
    avatar: "https://randomuser.me/api/portraits/women/3.jpg",
    profession: "Student | IIT",

    comment:
      "Special content! Long videos cover everything in detail. Excited for the next videos. Keep doing this amazing work!",
  },
  {
    name: "Rahat",
    avatar: "https://randomuser.me/api/portraits/women/4.jpg",
    profession: "Student | IIT",

    comment:
      "Join Binary physics for practical applications! Helpful lessons on creating projects from start to finish. Highly recommend to improve programming skills.",
  },
];

const Reviews = (props: Props) => {
  return (
    <div className="w-[90%]  m-auto">
      <div className="w-full 800px:flex items-center">
        <div className="w-full ">
          <h3 className={`${styles.title} 800px:!text-[40px]`}>
            Let's hear from <span className="text-gradient">the Students</span>{" "}
          </h3>
          <div className="w-1/2 mx-auto h-1 bg-gradient-to-r from-[#5046E5] to-transparent"></div>

          <br />
          <p className={`${styles.label} pb-10 px-16 text-center `}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque unde
            voluptatum dignissimos, nulla perferendis dolorem voluptate nemo
            possimus magni deleniti natus accusamus officiis quasi nihil
            commodi, praesentium quidem, quis doloribus? Lorem ipsum dolor sit
            amet consectetur adipisicing elit. Eaque unde voluptatum
            dignissimos, nulla perferendis dolorem voluptate nemo possimus magni
            deleniti natus accusamus officiis quasi nihil commodi, praesentium
            quidem, quis doloribus?
          </p>
        </div>
      </div>
      <div className="flex justify-center flex-wrap gap-[15px] md:gap-[20px] lg:gap-[25px] xl:gap-[30px] mb-12">
        {reviews &&
          reviews.map((i, index) => (
            <div key={index} className="w-full md:w-1/2 lg:w-1/4 ">
              <ReviewCard item={i} />
            </div>
          ))}
      </div>
    </div>
  );
};

export default Reviews;
