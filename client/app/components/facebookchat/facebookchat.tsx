import React from "react";
import { MessengerCustomerChat } from "typescript-react-facebook-messenger";

interface Props {}

export const Facebookchat = (props: Props) => {
  return (
  
    <div className="bg-red-800 w-[500px] z-100">

      <MessengerCustomerChat
        pageId="100064013891675"
        appId="930709311535335"
        themeColor="#0084ff"
      />
    </div>
  );
};
