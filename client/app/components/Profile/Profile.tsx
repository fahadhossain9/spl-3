"use client";
import React, { FC, useEffect, useState } from "react";
import SideBarProfile from "./SideBarProfile";
import { useLogOutQuery } from "../../../redux/features/auth/authApi";
import { signOut } from "next-auth/react";
import ProfileInfo from "./ProfileInfo";
import ChangePassword from "./ChangePassword";
import CourseCard from "../Course/CourseCard";
import { useGetUsersAllCoursesQuery } from "@/redux/features/courses/coursesApi";
import Upload from "@/app/upload/upload";
import AddExam from "@/app/addexam/page";
type Props = {
  user: any;
};

const Profile: FC<Props> = ({ user }) => {
  const [scroll, setScroll] = useState(false);
  const [avatar, setAvatar] = useState(null);
  const [logout, setLogout] = useState(false);
  const [courses, setCourses] = useState([]);
  const { data, isLoading } = useGetUsersAllCoursesQuery(undefined, {});

  const {} = useLogOutQuery(undefined, {
    skip: !logout ? true : false,
  });

  const [active, setActive] = useState(1);

  const logOutHandler = async () => {
    setLogout(true);
    await signOut();
  };

  if (typeof window !== "undefined") {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 85) {
        setScroll(true);
      } else {
        setScroll(false);
      }
    });
  }

  useEffect(() => {
    if (data) {
      const filteredCourses = user.courses
        .map((userCourse: any) =>
          data.courses.find((course: any) => course._id === userCourse._id)
        )
        .filter((course: any) => course !== undefined);
      setCourses(filteredCourses);
    }
  }, [data]);

  return (
    <div className="w-[85%] flex  mx-auto">
      <div
        className={` w-full md:w-1/2  pt-10 text-md md:text-lg  pb-6 px-8 dark:bg-slate-900 bg-opacity-90 border-2 bg-white dark:border-[#ffffff1d] max-h-[500px] border-[#5046e5] rounded-[15px] shadow-sm dark:shadow-sm mt-[80px] mb-[80px]  ${
          scroll ? "top-[120px]" : "top-[30px]"
        } left-[30px]`}
      >
        <SideBarProfile
          user={user}
          active={active}
          avatar={avatar}
          setActive={setActive}
          logOutHandler={logOutHandler}
        />
      </div>
      <div className="w-full md:w-1/2">
        {active === 1 && (
          <div className="w-full hover:text-xxl h-full bg-transparent mt-[80px]">
            <ProfileInfo avatar={avatar} user={user} />
          </div>
        )}

        {active === 2 && (
          <div className="w-full hover:text-xxl h-full bg-transparent mt-[80px]">
            <ChangePassword />
          </div>
        )}

        {active === 3 && (
          <div className="w-full hover:text-xxl pl-7 px-2 800px:px-10 800px:pl-8 mt-[80px]">
            <div className="flex flex-wrap justify-center gap-6 sm:justify-start sm:gap-8 lg:justify-between xl:justify-start">
              {courses &&
                courses.map((item: any, index: number) => (
                  <CourseCard item={item} key={index} />
                ))}
            </div>

            {courses.length === 0 && (
              <h1 className="text-center text-[18px] font-Poppins dark:text-white text-black">
                You don&apos;t have any purchased courses!
              </h1>
            )}
          </div>
        )}

        {active === 8 && (
          <div className="w-full hover:text-xxl h-full bg-transparent mt-[80px]">
            <Upload />
          </div>
        )}
      </div>
    </div>
  );
};

export default Profile;
