import Ratings from "@/app/utils/Ratings";
import Image from "next/image";
import Link from "next/link";
import React, { FC } from "react";
import { AiOutlineUnorderedList } from "react-icons/ai";
import { useTheme } from "next-themes";

type Props = {
  item: any;
  isProfile?: boolean;
};

const CourseCard: FC<Props> = ({ item, isProfile }) => {
  const { theme } = useTheme();

  return (
    <Link
      href={!isProfile ? `/course/${item._id}` : `course-access/${item._id}`}
    >
      <div className="relative bg-white dark:bg-[#112233] text-gray-800 dark:text-white rounded-lg shadow-lg overflow-hidden">
        <div className="h-60 bg-black">
          <Image
            src={item.thumbnail.url}
            width={500}
            height={300}
            objectFit="cover"
            className="object-cover object-center h-full w-full"
            alt=""
          />
        </div>
        <div className="p-4">
          <div className="flex justify-between items-center mb-2">
            <span className="text-sm h-10 text-gray-600 dark:text-white">
              {item.level} - {item.name}
            </span>
            <span className="text-sm text-yellow-500 font-bold">
              <Ratings rating={item.ratings} />
            </span>
          </div>
          <h2 className="text-lg font-semibold">{item.name}</h2>
          <p className="text-gray-600 dark:text-white">
            {item.price === 0 ? "Free" : `${item.price}$`}{" "}
            {item.price !== item.estimatedPrice && (
              <span className="line-through opacity-80 text-[14px]">
                {item.estimatedPrice}$
              </span>
            )}
          </p>
        </div>
        <div className="absolute  top-2 right-2 bg-white text-xs text-gray-800  px-2 pb-1 rounded">
          10 hours
        </div>
        <div className="w-full flex py-3 justify-between px-4 ">
          <div className="flex items-center">
            <AiOutlineUnorderedList
              size={20}
              fill={theme === "dark" ? "#fff" : "#000"}
            />{" "}
            <h5 className="pl-2 text-black font-semibold dark:text-[#fff]">
              {item.courseData?.length} Lectures
            </h5>
          </div>
          <h5
            className={`text-black dark:text-[#fff] font-semibold ${
              isProfile && "hidden 800px:inline"
            }`}
          >
            {item.purchased} Students
          </h5>
        </div>
      </div>
    </Link>
  );
};

export default CourseCard;
