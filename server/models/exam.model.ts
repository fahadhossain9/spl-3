import mongoose, { Schema, Document } from 'mongoose';

export interface Option {
  text: string;
  isCorrect: boolean;
}

export interface MCQ {
  question: string;
  options: Option[];
}

export interface Exam extends Document {
  course_Id: string;
  courseTitle: string;
  examTitle: string;
  mcqs: MCQ[];
}

const OptionSchema = new Schema({
  text: { type: String, required: true },
  isCorrect: { type: Boolean, required: true },
});

const MCQSchema = new Schema({
  question: { type: String, required: true },
  options: [OptionSchema],
});

const ExamSchema = new Schema({
  course_Id: { type: String, required: true },
  courseTitle: { type: String, },
  examTitle: { type: String, required: true },
  mcqs: [MCQSchema],
});

export default mongoose.model<Exam>('Exam', ExamSchema);
