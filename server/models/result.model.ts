import mongoose, { Schema, Document } from 'mongoose';

interface Option {
  id: string;
  text: string;
}

interface MCQResult {
  selected: string;
  correct: string[];
  question: string;
  options: Option[];
}

interface ResultData extends Document {
  course_Id: string;
  courseTitle: string;
  examTitle: string;
  userId: string;
  mcqs: Record<string, MCQResult>;
}

const OptionSchema = new Schema({
  id: String,
  text: String,
});

const MCQResultSchema = new Schema({
  selected: String,
  correct: [String],
  question: String,
  options: [OptionSchema],
});

const ResultDataSchema = new Schema({
  course_Id: String,
  courseTitle: String,
  examTitle: String,
  userId: String,
  mcqs: { type: Map, of: MCQResultSchema },
});

const ResultModel = mongoose.model<ResultData>('Result', ResultDataSchema);

export default ResultModel;
