// video.model.ts

import mongoose, { Document, Model, Schema } from "mongoose";

export interface IVideo extends Partial<Document> {
  title: string;
  url: string;
}

const videoSchema = new Schema<IVideo>(
  {
    title: {
      type: String,
      required: true,
    },
    url: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const VideoModel: Model<IVideo> = mongoose.model("Video", videoSchema);

export default VideoModel;
