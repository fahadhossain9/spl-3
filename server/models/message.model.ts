// message.model.ts

import mongoose, { Document, Model, Schema } from "mongoose";

export interface IMessage extends Partial<Document> {
  chatId?: string;
  userId: string;
  messages: Array<{ role: string; content: string }>;
}

const messageSchema = new Schema<IMessage>(
  {
    chatId: {
      type: String,
    },
    userId: {
      type: String,
      required: true,
    },
    messages: [
      {
        role: {
          type: String,
          required: true,
        },
        content: {
          type: String,
          required: true,
        },
      },
    ],
  },
  { timestamps: true }
);

const MessageModel: Model<IMessage> = mongoose.model("Message", messageSchema);

export default MessageModel;
