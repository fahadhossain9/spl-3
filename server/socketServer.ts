import { Server as SocketIOServer, Socket } from "socket.io";
import http from "http";

export const initSocketServer = (server: http.Server) => {
  const io = new SocketIOServer(server);

  io.on("connection", (socket: Socket) => {
    console.log("A user connected");
    console.log(`user ${socket.id} connected`);

    // Handle notification events
    socket.on("notification", (data) => {
      // Broadcast the notification data to all connected clients (admin dashboard)
      io.emit("newNotification", data);
    });

    // Handle chat events
    handleChatEvents(io, socket);

    socket.on("disconnect", () => {
      console.log("A user disconnected");
    });
  });
};

// initSocketServer.ts
const handleChatEvents = (io: SocketIOServer, socket: Socket) => {
  // Listen for 'chat message' event from the frontend
  socket.on(
    "chat message",
    (newMessage: { role: string; content: string; userId: string }) => {
      // Broadcast the chat message to all connected clients
      console.log(
        `Received message from ${newMessage.userId}: ${newMessage.content}`
      );
      io.emit("chat message", newMessage);
    }
  );
};
