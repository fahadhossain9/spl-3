// routes/message.routes.ts

import express from "express";
import { getUserMessages, getAdminMessages, createMessage } from "../controllers/message.controller";

const router = express.Router();
//create message
router.post("/messages",createMessage)
// Route for fetching messages for a specific user
router.get("/user/:userId/messages", getUserMessages);

// Route for fetching all messages for admin
router.get("/admin/messages", getAdminMessages);

export default router;
