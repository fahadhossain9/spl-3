// video.routes.ts

import express from "express";
import { createVideo, getAllVideos } from "../controllers/video.controller"; // Adjust the path accordingly
import multer from "multer";

const router = express.Router();

// Configure multer to handle file uploads
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "upload"); // Specify your upload folder path here
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage });

// POST - Create a new video
router.post("/upload", upload.single("videoFile"), createVideo);

// GET - Retrieve all videos
router.get("/allVideos", getAllVideos);

export default router;
