import express from 'express';
import examController from '../controllers/exam.controller';

const router = express.Router();

router.post('/createExam', examController.createExam);
router.get('/examlist', examController.getExams);
router.get('/getExam/:examId', examController.getExamById);
router.get('/course/:courseId', examController.getExamsByCourseId);

export default router;
