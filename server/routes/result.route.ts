import express from 'express';
import { getAllResultsForUser, postExamResult, getSpecificResult } from '../controllers/result.controller';

const resultRouter = express.Router();

// Route to get all result IDs for a given user
resultRouter.get('/results/ids/:userId', getAllResultsForUser);


// Route to get a specific result with result ID
resultRouter.get('/results/:resultId', getSpecificResult);

// Route to post exam results to the database
resultRouter.post('/postresults', postExamResult);

export default resultRouter;
