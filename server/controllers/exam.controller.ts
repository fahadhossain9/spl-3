import { Request, Response } from 'express';
import Exam from '../models/exam.model';

class ExamController {
  async createExam(req: Request, res: Response): Promise<void> {
    try {
      const { course_Id, courseTitle, examTitle, mcqs } = req.body;

      const exam = new Exam({
        course_Id,
        courseTitle,
        examTitle,
        mcqs,
      });

      const savedExam = await exam.save();

      res.status(201).json(savedExam);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  async getExams(_req: Request, res: Response): Promise<void> {
    try {
      const exams = await Exam.find();
      res.status(200).json(exams);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  async getExamById(req: Request, res: Response): Promise<void> {
    try {
      const examId = req.params.examId;
      const exam = await Exam.findById(examId);

      if (!exam) {
        res.status(404).json({ error: 'Exam not found' });
        return;
      }

      res.status(200).json(exam);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }

  async getExamsByCourseId(req: Request, res: Response): Promise<void> {
    try {
      const courseId = req.params.courseId;
      const exams = await Exam.find({ course_Id: courseId });

      res.status(200).json(exams);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }
}

export default new ExamController();
