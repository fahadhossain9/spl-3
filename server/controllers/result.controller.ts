import { Request, Response } from "express";
import ResultModel from "../models/result.model"; // Make sure to adjust the path accordingly

// Controller to post exam results to the database
export const postExamResult = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    const resultData = req.body;

    // Assuming you're using the Result model created earlier
    const result = new ResultModel(resultData);
    await result.save();

    res.status(201).json({ message: "Exam result submitted successfully!" });
  } catch (error) {
    console.error("Error submitting exam result:", error);
    res.status(500).send("Internal Server Error");
  }
};

// Controller to get a specific result with result ID
export const getSpecificResult = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    const id = req.params.resultId;

    const result = await ResultModel.findById(id);

    if (!result) {
      res.status(404).json({ message: "Result not found" });
      return;
    }

    res.status(200).json(result);
  } catch (error) {
    console.error("Error fetching specific result:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};
// Controller to get all exam result data for a given user
export const getAllResultsForUser = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    const userId = req.params.userId;

    // Find all results for the given user
    const results = await ResultModel.find({ userId });

    // Extract relevant information (id, examTitle, courseTitle) for each result
    const resultData = results.map((result) => ({
      id: result._id,
      examTitle: result.examTitle,
      courseTitle: result.courseTitle,
    }));

    res.status(200).json(resultData);
  } catch (error) {
    console.error("Error fetching result data:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};
