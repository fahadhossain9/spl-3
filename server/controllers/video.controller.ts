require("dotenv").config();
import { Request, Response, NextFunction } from "express";
import multer from "multer"; // Import multer
import VideoModel, { IVideo } from "../models/video.model";
import ErrorHandler from "../utils/ErrorHandler";
import { CatchAsyncError } from "../middleware/catchAsyncErrors";
import cloudinary from "cloudinary";
import { v2 as cloudinaryV2 } from "cloudinary";

// Configure Cloudinary
cloudinaryV2.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

// Configure multer to handle file uploads
// const storage = multer.memoryStorage(); // Store the file in memory
// const upload = multer({ storage: storage });

// POST - Create a new video with Cloudinary upload
export const createVideo =
  //   upload.single("videoFile"),
  CatchAsyncError(
    async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        const { title } = req.body;
        console.log(title);
        const videoFile = req.file; // Access the file from req.file

        // Check if required fields are present
        if (!title || !videoFile) {
          return next(
            new ErrorHandler("Title and video file are required", 400)
          );
        }

        // Upload video to Cloudinary
        console.log(videoFile);
        const cloudinaryUpload = await cloudinaryV2.uploader.upload(
          videoFile.path,
          {
            resource_type: "video",
            folder: "videos",
          }
        );

        // Create new video document with Cloudinary URL
        const newVideo: IVideo = {
          title,
          url: cloudinaryUpload.secure_url,
        };

        // Save the video document to MongoDB
        const createdVideo = await VideoModel.create(newVideo);

        res.status(201).json(createdVideo);
      } catch (error: any) {
        return next(new ErrorHandler(error.message, 400));
      }
    }
  );

// Add other video-related routes and functions as needed
// GET - Get all videos
export const getAllVideos = CatchAsyncError(
  async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      // Fetch all videos from the database
      const allVideos = await VideoModel.find();

      res.status(200).json(allVideos);
    } catch (error: any) {
      return next(new ErrorHandler(error.message, 400));
    }
  }
);
