// controllers/message.controller.ts

import { NextFunction, Request, Response } from "express";
import MessageModel, { IMessage } from "../models/message.model";

export const createMessage = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { userId, role, content } = req.body as {
      userId: string;
      role: string;
      content: string;
    };

    // Check if there is a chat with the given chatId and userId
    const existingChat = await MessageModel.findOne({ userId });

    if (existingChat) {
      // If the chat exists, push the new message to the existing messages array
      existingChat.messages.push({ role, content });
      await existingChat.save();
      res.status(201).json({ success: true, message: existingChat.messages });
    } else {
      // If the chat doesn't exist, create a new chat entry for the user
      const newChat: IMessage = {
        
        userId: userId,
        messages: [{ role, content }],
      };

      const createdChat = await MessageModel.create(newChat);
      res.status(201).json({ success: true, message: createdChat.messages });
    }
  } catch (error) {
    next(error);
  }
};
export const getUserMessages = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { userId } = req.params;

    // Fetch messages for the specified user ID
    const userMessages: IMessage | null = await MessageModel.findOne({
      userId,
    });

    if (userMessages) {
      res.status(200).json({ success: true, messages: userMessages.messages });
    } else {
      res.status(404).json({ success: false, message: "User not found" });
    }
  } catch (error) {
    next(error);
  }
};

export const getAdminMessages = async (
  _req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    // Fetch all unique user IDs
    const uniqueUserIds: string[] = await MessageModel.distinct("userId");

    // Fetch all messages for each user
    const adminMessages: {
      userId: string;
      messages: Array<{ role: string; content: string }>;
    }[] = [];

    for (const userId of uniqueUserIds) {
      const userMessages: IMessage | null = await MessageModel.findOne({
        userId,
      });

      if (userMessages) {
        adminMessages.push({ userId, messages: userMessages.messages });
      }
    }

    res.status(200).json({ success: true, messages: adminMessages });
  } catch (error) {
    next(error);
  }
};
